package com.codespot.printconnector.model;

import lombok.Data;

@Data
public class Printer {
    private int id;
    private String name;

    public Printer(int id, String name) {
        this.id = id;
        this.name = name;
    }
}

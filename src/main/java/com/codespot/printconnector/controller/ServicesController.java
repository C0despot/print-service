package com.codespot.printconnector.controller;

import com.codespot.printconnector.model.Invoice;
import com.codespot.printconnector.model.PrinterConfig;
import com.codespot.printconnector.model.Status;
import com.codespot.printconnector.service.print.DefaultPrintService;
import com.codespot.printconnector.service.print.PrintService;
import com.codespot.printconnector.service.print.job.PrintJob;
import com.itextpdf.zugferd.exceptions.DataIncompleteException;
import com.itextpdf.zugferd.exceptions.InvalidCodeException;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.print.PrinterException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
@Log
@RequestMapping(value = "service")
public class ServicesController {

    private final
    PrintService printService;

    private String serverPort;
    private String resourcePath;
    private String printer;

    @Autowired
    public ServicesController(DefaultPrintService printService,
                              @Value("${server.port}")String serverPort,
                              @Value("${service.resource.path}")String resourcePath,
                              @Value("${service.printer.pos}")String printer) {
        this.serverPort = serverPort;
        this.resourcePath = resourcePath;
        this.printer = printer;
        this.printService = printService;
    }

    @PostMapping(value = "printer/invoice")
    private ResponseEntity printInvoice(@RequestBody Invoice invoice) throws DataIncompleteException, ParseException, TransformerException, IOException, ParserConfigurationException, InvalidCodeException, SAXException, PrinterException {
        return this.printService.doPrintPosInvoice(invoice);
    }

    @PostMapping(value = "printer/invoice/full")
    private ResponseEntity printFullInvoice(@RequestBody Invoice invoice) throws DataIncompleteException, ParseException, TransformerException, IOException, ParserConfigurationException, InvalidCodeException, SAXException, PrinterException {
        return this.printService.doPrintFullInvoice(invoice);
    }

    @GetMapping(value = "status")
    ResponseEntity getAppStatus(){
        return ResponseEntity.ok(getCurrentAppStatus());
    }

    private Status getCurrentAppStatus(){
        String status = "RUNNING";
        List<String> messages = new ArrayList<>();
        if (PrintJob.findPrintService(printer) == null) {
            status = "FAIL";
            messages.add("Cannot connect to printer.");
        }
        if (Files.notExists(Paths.get(resourcePath))){
            status = "FAIL";
            messages.add("Path for printer's docs does not exist.");
        }
        if (status.equalsIgnoreCase("RUNNING"))
            messages.add("Everything is working perfectly");

        return new Status(serverPort, resourcePath, printer, messages, status);
    }

    @GetMapping(value = "printers")
    ResponseEntity getPrinters(String term){
        return this.printService.getAvailablePrinters(term);
    }

    @PostMapping(value = "printer")
    ResponseEntity savePrinterConfig(@RequestBody PrinterConfig printerConfig) throws IOException {
        return printService.saveConfig(printerConfig);
    }

    @GetMapping(value = "printer")
    ResponseEntity retrievePrinterConfig() throws IOException {
        return printService.getConfig();
    }

}

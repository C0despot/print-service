package com.codespot.printconnector.model;

import lombok.Data;

import java.util.Date;

@Data
public class User {

    public User() {
    }

    private Long userId;

    private String username;

    private String address;

    private String phoneNumber;

    private String email;

    private String firstName;

    private String lastName;

    private Long age;

    private Date birthDate;

    private String password;

    private Boolean validated;

    private String bloodType;

    private String bankAccountNumber;

    private Status status;

    private Boolean locked;

    private Date joinDate;

    private Date fireDate;

    private Date createdDate;

    private Date modifiedDate;

    private User createdBy;

    private User modifiedBy;

    private String initialSalary;

    private String currentSalary;

    private String idCard;

    private Boolean driverPermit;

    private String married;

    private Integer children;

    private String wife;

    private String nationality;

    private String gender;
}

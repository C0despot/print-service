package com.codespot.printconnector.model;

import lombok.Data;

@Data
public class StatusDto {
    public StatusDto(){}
    private Long id;
    private String description;
}

package com.codespot.printconnector.listener;

import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.JobName;
import javax.print.attribute.standard.PrinterName;
import javax.print.event.PrintJobAdapter;
import javax.print.event.PrintJobEvent;
import java.awt.*;
import java.awt.print.PrinterJob;
import java.io.*;
import java.util.logging.Logger;

@Component
public class CashDrawerListener implements
        ApplicationListener<ContextRefreshedEvent> {

    private static final Logger LOG = Logger.getLogger(CashDrawerListener.class.getName());
    private static int counter;
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        LOG.info("Increment counter " + counter);
        counter++;
    }


//    public void cashdrawerOpen() {
//
//        byte[] open = {27, 112, 48, 55, 121};
////        byte[] cutter = {29, 86,49};
//        String printer = "Microsoft Print to PDF";
//        PrintServiceAttributeSet printserviceattributeset = new HashPrintServiceAttributeSet();
//        printserviceattributeset.add(new PrinterName(printer,null));
//        PrintService[] printservice = PrintServiceLookup.lookupPrintServices(null, printserviceattributeset);
//        if(printservice.length!=1){
//            LOG.severe("Printer not found");
//        }
//        PrintService pservice = printservice[0];
//        DocPrintJob job = pservice.createPrintJob();
//        DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
//        Doc doc = new SimpleDoc(open,flavor,null);
//        PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
//        aset.add(new JobName( "Document", null));
//        try {
//            job.print(doc, aset);
//        } catch (PrintException ex) {
//            LOG.severe(ex.getMessage());
//        }
//    }


}

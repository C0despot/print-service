package com.codespot.printconnector.service.print;

import com.codespot.printconnector.model.Invoice;
import com.codespot.printconnector.model.PrinterConfig;
import com.codespot.printconnector.process.PrintPosProcess;
import com.codespot.printconnector.process.PrintProcess;
import com.codespot.printconnector.service.print.job.PrintJob;
import com.itextpdf.zugferd.exceptions.DataIncompleteException;
import com.itextpdf.zugferd.exceptions.InvalidCodeException;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.print.PrinterException;
import java.io.*;
import java.text.ParseException;
import java.util.Date;
import java.util.Properties;


@Service(value = "defaultPrintService")
@Log
public class DefaultPrintService implements PrintService {

    private final
    PrintJob printJob;

    @Value("${service.resource.path}")
    private String path;

    @Autowired
    public DefaultPrintService(PrintJob printJob) {
        this.printJob = printJob;
    }

    @Override
    public ResponseEntity doPrintPosInvoice(Invoice invoice) throws DataIncompleteException, ParseException, TransformerException, SAXException, InvalidCodeException, ParserConfigurationException, IOException, PrinterException {
        PrintPosProcess posProcess = new PrintPosProcess();
        String invoicePdf = posProcess.createPdf(invoice, getRealPath());
        this.printJob.printPosJob(invoicePdf);
        return null;
    }

    @Override
    public ResponseEntity doPrintFullInvoice(Invoice invoice) throws DataIncompleteException, ParseException, TransformerException, SAXException, InvalidCodeException, ParserConfigurationException, IOException, PrinterException {
        PrintProcess printProcess = new PrintProcess();
        String invoicePdf = printProcess.createPdf(invoice, getRealPath());
        this.printJob.printJob(invoicePdf);
        return null;
    }

    @Override
    public ResponseEntity getAvailablePrinters(String term) {
        return ResponseEntity.ok(this.printJob.getPrintList(term));
    }

    @Override
    public ResponseEntity saveConfig(PrinterConfig config) throws IOException {
        if (config.getDirectory().charAt(config.getDirectory().length() - 1) != '/')
            config.setDirectory(config.getDirectory() + '/');
        Properties properties = new Properties();
        OutputStream fileOutputStream = new FileOutputStream(path + "printer.config.properties");
        properties.setProperty("printer.pos", config.getPos());
        properties.setProperty("printer.basic", config.getBasic());
        properties.setProperty("printer.modifier", config.getModifier());
        properties.setProperty("printer.path", config.getDirectory());
        properties.store(fileOutputStream, null);
        return ResponseEntity.ok(properties);
    }

    @Override
    public ResponseEntity getConfig() throws IOException {
        Properties prop = new Properties();
        InputStream input = new FileInputStream(path + "printer.config.properties");
        prop.load(input);
        return ResponseEntity.ok(prop);
    }

    private String getRealPath() {
        try {
            Properties prop = new Properties();
            InputStream input = new FileInputStream(path + "printer.config.properties");
            prop.load(input);
            String path = prop.getProperty("printer.path");
            if (path != null && !path.equals(""))
                return path;
            return this.path;
        } catch (Exception ignored) {
            return this.path;
        }
    }
}

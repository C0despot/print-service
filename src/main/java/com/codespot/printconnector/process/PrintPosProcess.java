package com.codespot.printconnector.process;


import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import com.codespot.printconnector.model.Customer;
import com.codespot.printconnector.model.Invoice;
import com.codespot.printconnector.model.PosProduct;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.DashedBorder;
import com.itextpdf.zugferd.ZugferdConformanceLevel;
import com.itextpdf.zugferd.ZugferdDocument;
import com.itextpdf.zugferd.exceptions.InvalidCodeException;
import org.xml.sax.SAXException;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfOutputIntent;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import com.itextpdf.zugferd.exceptions.DataIncompleteException;


public class PrintPosProcess {

    /**
     * The pattern of the destination paths.
     */
    private String DEST = "basic%05d.pdf";

    /**
     * The path to the color profile.
     */
    private String ICC = "assets/color/sRGB_CS_profile.icm";

    /**
     * The path to a regular font.
     */
    private String REGULAR = "assets/fonts/OpenSans-Regular.ttf";

    /**
     * The path to a bold font.
     */
    private String BOLD = "assets/fonts/OpenSans-Bold.ttf";

    /**
     * A <code>String</code> with a newline character.
     */
    private final String NEWLINE = "\n";

    private void setPaths(String basePath) throws IOException {
        this.DEST = basePath + "pdf/" + this.DEST;
        this.ICC = basePath + this.ICC;
        this.REGULAR = basePath + this.REGULAR;
        this.BOLD = basePath + this.BOLD;
    }

    public String createPdf(Invoice invoice, String path) throws ParserConfigurationException, SAXException, TransformerException, IOException, ParseException, DataIncompleteException, InvalidCodeException {

        this.setPaths(path);
        String dest = String.format(DEST, invoice.getInvoiceId());

        // Create the ZUGFeRD document
        ZugferdDocument pdfDocument = new ZugferdDocument(
                new PdfWriter(dest), ZugferdConformanceLevel.ZUGFeRDBasic,
                new PdfOutputIntent("Custom", "", "http://www.color.org",
                        "sRGB IEC61966-2.1", new FileInputStream(ICC)));
        // Create the document
        PageSize pageSize = new PageSize(220, 420 + invoice.getProducts().size() * 46);
        Document document = new Document(pdfDocument, pageSize);
        document.setFont(PdfFontFactory.createFont(REGULAR, true))
                .setFontSize(12);
        document.add(getCompanyName());
        document.add(getAddress(invoice.getCustomer(), invoice.getVoucherNo()));
        document.add(getInvoiceAndDate(invoice));
        document.add(getProductHeader());
        document.add(getLineItemTable(invoice));
        document.add(getTotals(invoice));
        document.add(getAttendances(invoice));
        document.add(getFootMsg());
        document.add(getCuttingLine());

        document.close();
        return dest;
    }


    private String getDateString(Date d) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy h:mm:ss a");
        return sdf.format(d);
    }

    private Table getCompanyName() {
        Table table = new Table(new UnitValue[]{
                new UnitValue(UnitValue.PERCENT, 100)});
        table.setMarginLeft(-20)
                .setWidth(185)
                .setBorder(Border.NO_BORDER);
        table.setFontSize(10);
        table.addCell(createCell("RV Imperio Electrico, SRL")
                .setTextAlignment(TextAlignment.CENTER));
        return table;
    }

    private Table getAttendances(Invoice invoice) {
        return (new Table(new UnitValue[]{
                new UnitValue(UnitValue.PERCENT, 100)})
                .setMarginLeft(-20)
                .setMarginTop(50)
                .setWidth(185)
                .setBorder(Border.NO_BORDER)
                .setFontSize(10)
                .addCell(createCell("Le Atendio: ")
                        .setTextAlignment(TextAlignment.CENTER))
                .addCell(createCell(invoice.getCreatedBy().getFirstName() + " " + invoice.getCreatedBy().getLastName())
                        .setTextAlignment(TextAlignment.CENTER)));
    }

    private Table getInvoiceAndDate(Invoice invoice) throws ParseException {
        return (new Table(new UnitValue[]{
                new UnitValue(UnitValue.PERCENT, 45),
                new UnitValue(UnitValue.PERCENT, 55)})
                .setMarginTop(15)
                .setMarginLeft(-20)
                .setMarginRight(0)
                .setWidth(185)
                .setBorder(Border.NO_BORDER)
                .setFontSize(8)
                .setBorderTop(new DashedBorder(0.5f))
                .addCell(createCell("Factura: " + invoice.getInvoiceId())
                        .setTextAlignment(TextAlignment.LEFT))
                .addCell(createCell(this.getDateString(invoice.getDate())))
                .setTextAlignment(TextAlignment.RIGHT));
    }

    private Table getFootMsg() {
        return (new Table(new UnitValue[]{
                new UnitValue(UnitValue.PERCENT, 100)})
                .setMarginLeft(-20)
                .setMarginRight(20)
                .setMarginBottom(40)
                .setWidth(185)
                .setMarginTop(15)
                .setFontSize(10)
                .addCell(createCell("¡¡¡Gracias Por Su Compra!!!")
                        .setTextAlignment(TextAlignment.CENTER))
                .setBorderBottom(new DashedBorder(0.5f))
                .setBorderTop(new DashedBorder(0.5f)));
    }

    private Table getCuttingLine() {
        return (new Table(new UnitValue[]{
                new UnitValue(UnitValue.PERCENT, 100)})
                .setMarginBottom(0)
                .setMarginLeft(-20)
                .setWidth(185)
                .setMarginTop(20)
                .setFontSize(10)
                .setBorderTop(new DashedBorder(0.5f)));
    }

    private Table getProductHeader() {
        return (new Table(new UnitValue[]{
                new UnitValue(UnitValue.PERCENT, 15),
                new UnitValue(UnitValue.PERCENT, 60),
                new UnitValue(UnitValue.PERCENT, 25)})
                .setMarginTop(15)
                .setMarginLeft(-20)
                .setMarginRight(20)
                .setWidth(185)
                .setBorder(Border.NO_BORDER)
                .setFontSize(10)
                .addCell(createCell("Cant.")
                        .setTextAlignment(TextAlignment.LEFT))
                .setFontSize(9)
                .setBorderBottom(new DashedBorder(0.5f))
                .setBorderTop(new DashedBorder(0.5f))
                .addCell(createCell("Descripcion")
                        .setTextAlignment(TextAlignment.LEFT))
                .setFontSize(9)
                .setBorderBottom(new DashedBorder(0.5f))
                .setBorderTop(new DashedBorder(0.5f))
                .addCell(createCell("Total")
                        .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(9)
                .setBorderBottom(new DashedBorder(0.5f))
                .setBorderTop(new DashedBorder(0.5f)));
    }

    private Table getAddress(Customer customer, String voucher) {
        Table table = new Table(new UnitValue[]{
                new UnitValue(UnitValue.PERCENT, 60),
                new UnitValue(UnitValue.PERCENT, 40)})
                .setMarginTop(15)
                .setMarginLeft(-20)
                .setMarginRight(20)
                .setWidth(185)
                .setBorder(Border.NO_BORDER)
                .setFontSize(10)
                .addCell(createCell("" +
                        "Av. Isabel Aguiar 84\n" +
                        "Tel. 809-531-4422\n" +
                        "RNC: 130528268\n" +
                        "NCF: " + voucher + "\n")
                        .setTextAlignment(TextAlignment.LEFT))
                .setFontSize(8);

        String clientObj = customer.getName() +
                (customer.getRnc() == null || customer.getRnc().equals("") ? NEWLINE : ("\nRNC: " + customer.getRnc()) + NEWLINE) +
                (customer.getAddress() == null || customer.getAddress().equals("") ? NEWLINE : ("Direccion: " + customer.getAddress()) + NEWLINE);
        table.addCell(createCell(clientObj)
                .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(8);

        return table;
    }

    private Table getTotals(Invoice invoice) {
        return (new Table(new UnitValue[]{
                new UnitValue(UnitValue.PERCENT, 70),
                new UnitValue(UnitValue.PERCENT, 40)})
                .setBorderTop(new DashedBorder(0.5f))
                .setMarginTop(0)
                .setMarginLeft(-20)
                .setMarginRight(20)
                .setWidth(185)
                .setBorder(Border.NO_BORDER)
                .setFontSize(10)
                .addCell(createCell("SubTotal")
                        .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(9)
                .addCell(createCell(this.getCurrencyValue(invoice.getSubTotal()))
                        .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(9)
                .addCell(createCell("Descuento")
                        .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(9)
                .addCell(createCell(this.getCurrencyValue(invoice.getDiscountTtl()))
                        .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(9)
                .addCell(createCell("ITBIS")
                        .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(9)
                .addCell(createCell(this.getCurrencyValue(invoice.getTax()))
                        .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(9)
                .addCell(createCell("Total")
                        .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(9)
                .addCell(createCell(this.getCurrencyValue(invoice.getTotal()))
                        .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(9));
    }

    private Table getLineItemTable(Invoice invoice) {
        Table table = new Table(new UnitValue[]{
                new UnitValue(UnitValue.PERCENT, 15),
                new UnitValue(UnitValue.PERCENT, 60),
                new UnitValue(UnitValue.PERCENT, 25)})
                .setMarginTop(10)
                .setMarginBottom(5)
                .setMarginLeft(-20)
                .setMarginRight(20)
                .setWidth(185)
                .setBorder(Border.NO_BORDER)
                .setFontSize(9);

        for (PosProduct product : invoice.getProducts()) {
            table.addCell(createCell(String.valueOf(product.getQty()))
                    .setTextAlignment(TextAlignment.LEFT))
                    .setFontSize(9)
                    .addCell(createCell(
                            this.capitalize(product.getPosDescription() +
                                    " Precio: " + this.getCurrencyValue(product.getPrice()) +
                                    " Codigo: " + product.getCode())))
                    .setFontSize(9)
                    .setTextAlignment(TextAlignment.LEFT)
                    .addCell(createCell(
                            this.getCurrencyValue(product.getPrice() * product.getQty()))
                            .setTextAlignment(TextAlignment.RIGHT));
        }
        return table;
    }

    private String capitalize(final String line) {
        return Character.toUpperCase(line.toLowerCase().charAt(0)) + line.toLowerCase().substring(1);
    }

    private Cell createCell(String text) {
        return new Cell().setPadding(0.8f)
                .setBorder(Border.NO_BORDER)
                .add(new Paragraph(text)
                        .setMultipliedLeading(1));
    }

    private String getCurrencyValue(Double amount) {
        return String.format("%,.2f", amount);
    }


}

package com.codespot.printconnector.model;

import lombok.Data;

import java.util.List;

@Data
public class Status {

    public Status() {
    }

    private String serverPort;
    private String resourcePath;
    private String printerName;
    private List<String> messages;
    private String status;

    public Status(String serverPort, String resourcePath, String printerName, List<String> messages, String status) {
        this.serverPort = serverPort;
        this.resourcePath = resourcePath;
        this.printerName = printerName;
        this.messages = messages;
        this.status = status;
    }
}

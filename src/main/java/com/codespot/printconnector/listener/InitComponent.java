package com.codespot.printconnector.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Component
public class InitComponent {

    private static final String TASKLIST = "tasklist";
    private static final String KILL = "taskkill /F /IM ";
    private static final String PIDLIST = "netstat -a -o -n";

    @Autowired
    public InitComponent(@Value("${server.port}")String serverPort) throws IOException {
        String pid = this.findPid(serverPort);
        if (pid != null && isProcessRunning(pid)) {
            killProcess(pid);
        }
    }

    private boolean isProcessRunning(String serviceName) throws IOException {
        Process p = Runtime.getRuntime().exec(TASKLIST);
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            if (line.contains(serviceName)) {
                return true;
            }
        }
        return false;
    }

    private String findPid(String port) throws IOException {
        Process p = Runtime.getRuntime().exec(PIDLIST);
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        while (((line = reader.readLine()) != null)) {
            line = line.replace(" ", ",");
            if (line.contains(port)) {
                String[] lineArr = line.split(",");
                return lineArr[lineArr.length-1];
            }
        }
        return null;
    }
    private void killProcess(String serviceName) throws IOException {
        Runtime.getRuntime().exec(KILL + serviceName);
    }
}

package com.codespot.printconnector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrintconnectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrintconnectorApplication.class, args);
	}
}

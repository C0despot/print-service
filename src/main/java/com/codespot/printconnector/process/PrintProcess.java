package com.codespot.printconnector.process;

import com.codespot.printconnector.model.Customer;
import com.codespot.printconnector.model.Invoice;
import com.codespot.printconnector.model.PosProduct;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfOutputIntent;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.DashedBorder;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import com.itextpdf.zugferd.ZugferdConformanceLevel;
import com.itextpdf.zugferd.ZugferdDocument;
import com.itextpdf.zugferd.exceptions.DataIncompleteException;
import com.itextpdf.zugferd.exceptions.InvalidCodeException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PrintProcess {

    /**
     * The pattern of the destination paths.
     */
    private String DEST = "basic%05d.pdf";

    /**
     * The path to the color profile.
     */
    private String ICC = "assets/color/sRGB_CS_profile.icm";

    /**
     * The path to a regular font.
     */
    private String REGULAR = "assets/fonts/OpenSans-Regular.ttf";

    /**
     * The path to a bold font.
     */
    private String BOLD = "assets/fonts/OpenSans-Bold.ttf";

    /**
     * A <code>String</code> with a newline character.
     */
    private final String NEWLINE = "\n";

    private void setPaths(String basePath) {
        this.DEST = basePath + "pdf/" + this.DEST;
        this.ICC = basePath + this.ICC;
        this.REGULAR = basePath + this.REGULAR;
        this.BOLD = basePath + this.BOLD;
    }

    public String createPdf(Invoice invoice, String path) throws ParserConfigurationException, SAXException, TransformerException, IOException, ParseException, DataIncompleteException, InvalidCodeException {

        this.setPaths(path);
        String dest = String.format(DEST, invoice.getInvoiceId());

        // Create the ZUGFeRD document
        ZugferdDocument pdfDocument = new ZugferdDocument(
                new PdfWriter(dest), ZugferdConformanceLevel.ZUGFeRDBasic,
                new PdfOutputIntent("Custom", "", "http://www.color.org",
                        "sRGB IEC61966-2.1", new FileInputStream(ICC)));
        ;
        // Create the document
//        PageSize pageSize = new PageSize(200, 320 + invoice.getProducts().size() * 46);
        Document document = new Document(pdfDocument);
        document.setFont(PdfFontFactory.createFont(REGULAR, true))
                .setFontSize(12);
        document.add(getCompanyName());
        document.add(getAddress(invoice.getCustomer()));
        document.add(getInvoiceAndDate(invoice));
        document.add(getProductHeader());
        document.add(getLineItemTable(invoice));
        document.add(getTotals(invoice));
//        document.add(getFootMsg());

        document.close();
        return dest;
    }


    private String getDateString(Date d, String newFormat) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(newFormat);
        return sdf.format(d);
    }

    private Table getCompanyName() {
        return new Table(new UnitValue[]{
                new UnitValue(UnitValue.PERCENT, 100)})
                .setMarginLeft(0)
                .setWidth(520)
                .setBorder(Border.NO_BORDER)
                .setFontSize(16)
                .addCell(createCell("RV Imperio Electrico, SRL")
                .setTextAlignment(TextAlignment.CENTER));
    }

    private Table getInvoiceAndDate(Invoice invoice) throws ParseException {
        String type = invoice.getStatus().isDraft() ? "Cotización" : "Factura";
        return (new Table(new UnitValue[]{
                new UnitValue(UnitValue.PERCENT, 45),
                new UnitValue(UnitValue.PERCENT, 55)})
                .setMarginTop(40)
                .setMarginLeft(0)
                .setMarginRight(0)
                .setWidth(520)
                .setBorder(Border.NO_BORDER)
                .setFontSize(12)
                .setBorderTop(new DashedBorder(0.5f))
                .addCell(createCell(type + ": " + invoice.getInvoiceId())
                        .setTextAlignment(TextAlignment.LEFT))
                .addCell(createCell(this.getDateString(invoice.getDate(), "d MMM yyyy h:mm:ss a")))
                .setTextAlignment(TextAlignment.RIGHT));
    }

    private Table getFootMsg() {
        Table table = new Table(new UnitValue[]{
                new UnitValue(UnitValue.PERCENT, 100)})
                .setMarginLeft(0)
                .setMarginRight(0)
                .setWidth(520)
                .setMarginTop(15)
                .setFontSize(12)
                .addCell(createCell("¡¡¡Gracias Por Su Compra!!!")
                        .setTextAlignment(TextAlignment.CENTER))
                .setBorderBottom(new DashedBorder(0.5f))
                .setBorderTop(new DashedBorder(0.5f));
        ;
        return table;
    }

    private Table getProductHeader() {
        Table table = new Table(new UnitValue[]{
                new UnitValue(UnitValue.PERCENT, 15),
                new UnitValue(UnitValue.PERCENT, 40),
                new UnitValue(UnitValue.PERCENT, 15),
                new UnitValue(UnitValue.PERCENT, 15),
                new UnitValue(UnitValue.PERCENT, 15)})
                .setMarginTop(40)
                .setMarginLeft(0)
                .setMarginRight(20)
                .setWidth(520)
                .setBorder(Border.NO_BORDER);
        table.setFontSize(12);
        table.addCell(createCell("Codigo")
                .setTextAlignment(TextAlignment.LEFT))
                .setFontSize(12)
                .setBorderBottom(new SolidBorder(0.5f))
                .setBorderTop(new SolidBorder(0.5f));

        table.addCell(createCell("Descripcion")
                .setTextAlignment(TextAlignment.LEFT))
                .setFontSize(12)
                .setBorderBottom(new SolidBorder(0.5f))
                .setBorderTop(new SolidBorder(0.5f));

        table.addCell(createCell("Cantidad")
                .setTextAlignment(TextAlignment.LEFT))
                .setFontSize(12)
                .setBorderBottom(new SolidBorder(0.5f))
                .setBorderTop(new SolidBorder(0.5f));
        table.addCell(createCell("Precio")
                .setTextAlignment(TextAlignment.LEFT))
                .setFontSize(12)
                .setBorderBottom(new SolidBorder(0.5f))
                .setBorderTop(new SolidBorder(0.5f));

        table.addCell(createCell("Total")
                .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(12)
                .setBorderBottom(new SolidBorder(0.5f))
                .setBorderTop(new SolidBorder(0.5f));
        return table;
    }

    private Table getAddress(Customer customer) {
        Table table = new Table(new UnitValue[]{
                new UnitValue(UnitValue.PERCENT, 60),
                new UnitValue(UnitValue.PERCENT, 40)})
                .setMarginTop(50);
        table.setMarginLeft(0)
                .setMarginRight(0)
                .setWidth(520)
                .setBorder(Border.NO_BORDER);
        table.setFontSize(1);

        table.addCell(createCell("" +
                "Av. Isabel Aguiar 84\n" +
                "Tel. 809-531-4422\n" +
                "RNC: 130528268\n")
                .setTextAlignment(TextAlignment.LEFT))
                .setFontSize(12);

        String clientObj = customer.getName() +
                (customer.getRnc() == null || customer.getRnc().equals("") ? NEWLINE : ("\nRNC: " + customer.getRnc()) + NEWLINE) +
                (customer.getAddress() == null || customer.getAddress().equals("") ? NEWLINE : ("Direccion: " + customer.getAddress()) + NEWLINE);
        table.addCell(createCell(clientObj)
                .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(12);

        return table;
    }

    private Table getTotals(Invoice invoice) {
        return new Table(new UnitValue[]{
                new UnitValue(UnitValue.PERCENT, 70),
                new UnitValue(UnitValue.PERCENT, 40)})
                .setBorderTop(new SolidBorder(0.5f))
                .setMarginTop(0)
                .setMarginLeft(0)
                .setMarginRight(0)
                .setWidth(520)
                .setBorder(Border.NO_BORDER)
                .setFontSize(12)
                .addCell(createCell("SubTotal")
                        .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(12)
                .addCell(createCell(this.getCurrencyValue(invoice.getSubTotal()))
                        .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(12)
                .addCell(createCell("Descuento")
                        .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(12)
                .addCell(createCell(this.getCurrencyValue(invoice.getDiscountTtl()))
                        .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(12)
                .addCell(createCell("ITBIS")
                        .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(12)
                .addCell(createCell(this.getCurrencyValue(invoice.getTax()))
                        .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(12)
                .addCell(createCell("Total")
                        .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(12)
                .addCell(createCell(this.getCurrencyValue(invoice.getTotal()))
                        .setTextAlignment(TextAlignment.RIGHT))
                .setFontSize(12);
    }

    private Table getLineItemTable(Invoice invoice) {
        Table table = new Table(new UnitValue[]{
                new UnitValue(UnitValue.PERCENT, 15),
                new UnitValue(UnitValue.PERCENT, 40),
                new UnitValue(UnitValue.PERCENT, 15),
                new UnitValue(UnitValue.PERCENT, 15),
                new UnitValue(UnitValue.PERCENT, 15)})
                .setMarginTop(0)
                .setMarginBottom(5)
                .setMarginLeft(0)
                .setMarginRight(0)
                .setWidth(520)
                .setBorder(Border.NO_BORDER)
                .setFontSize(12);

        for (PosProduct product : invoice.getProducts()) {
            table.setFontSize(12)
                    .addCell(createCell(product.getCode()))
                    .addCell(createCell(product.getFullDescription()))
                    .addCell(createCell(getCurrencyValue(product.getQty())))
                    .addCell(createCell(getCurrencyValue(product.getPrice())))
                    .addCell(createCell(
                            this.getCurrencyValue(product.getPrice() * product.getQty()))
                            .setTextAlignment(TextAlignment.RIGHT));
        }
        return table;
    }

    private String capitalize(final String line) {
        return Character.toUpperCase(line.toLowerCase().charAt(0)) + line.toLowerCase().substring(1);
    }

    private Cell createCell(String text) {
        return new Cell().setPadding(0.8f)
                .setBorder(Border.NO_BORDER)
                .add(new Paragraph(text)
                        .setMultipliedLeading(1));
    }

    private String getCurrencyValue(Double amount) {
        return String.format("%,.2f", amount);
    }


}

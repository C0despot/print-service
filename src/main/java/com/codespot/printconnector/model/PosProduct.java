package com.codespot.printconnector.model;

import lombok.Data;

import java.util.Arrays;
import java.util.List;

@Data
public class PosProduct {

    private Long docId;

    private String description;

    private Double price;

    private Double qty;

    private Long id;

    private Long invoice;

    private String code;

    public String getFullDescription(){
        return description;
    }
    public String getPosDescription(){
        String description = this.description;
        List<String> descList = Arrays.asList(description.split(" "));
        final String[] result = {""};
        descList.forEach((desc)->{
            if (desc.length()> 6)
                desc = desc.substring(0, 6);
            result[0] += desc + " ";
        });
        return result[0];
    }
}
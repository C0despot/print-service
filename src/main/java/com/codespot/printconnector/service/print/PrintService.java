package com.codespot.printconnector.service.print;

import com.codespot.printconnector.model.Invoice;
import com.codespot.printconnector.model.PrinterConfig;
import com.itextpdf.zugferd.exceptions.DataIncompleteException;
import com.itextpdf.zugferd.exceptions.InvalidCodeException;
import org.springframework.http.ResponseEntity;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.print.PrinterException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

public interface PrintService {

    ResponseEntity doPrintPosInvoice(Invoice invoice) throws DataIncompleteException, ParseException, TransformerException, SAXException, InvalidCodeException, ParserConfigurationException, IOException, PrinterException;
    ResponseEntity doPrintFullInvoice(Invoice invoice) throws DataIncompleteException, ParseException, TransformerException, SAXException, InvalidCodeException, ParserConfigurationException, IOException, PrinterException;
    ResponseEntity getAvailablePrinters(String term);
    ResponseEntity saveConfig(PrinterConfig config) throws IOException;
    ResponseEntity getConfig() throws IOException;
}

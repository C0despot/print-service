package com.codespot.printconnector.model;


import lombok.Data;

@Data
public class Customer {

    private Long id;

    private String name;

    private String address;

    private String clientType;

    private String allowCredit;

    private Double balance;

    private String phone;

    private String rnc;
}

package com.codespot.printconnector.service.print.job;

import com.codespot.printconnector.model.Printer;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.JobName;
import javax.print.attribute.standard.PrinterName;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

@Service
public class PrintJob {

    @Value("${service.printer.pos}")
    private String posPrinter;

    @Value("${service.printer}")
    private String printer;

    @Value(value = "${service.resource.path}")
    private String path;

    private static final Logger LOG = Logger.getLogger(PrintJob.class.getName());

    public void printPosJob(String file) throws PrinterException, IOException {
        setPrinters();
        PDDocument document = PDDocument.load(new File(file));
        PrintService myPrintService = findPrintService(this.posPrinter);
        PrinterJob job = PrinterJob.getPrinterJob();
        job.setPageable(new PDFPageable(document));
        job.setPrintService(myPrintService);
        this.cashDrawerOpen();
        job.print();
        job.print();

    }

    public void printJob(String file) throws PrinterException, IOException {

        setPrinters();
        PDDocument document = PDDocument.load(new File(file));
        PrintService myPrintService = findPrintService(this.printer);
        PrinterJob job = PrinterJob.getPrinterJob();
        job.setPageable(new PDFPageable(document));
        job.setPrintService(myPrintService);
        this.cashDrawerOpen();
        job.print();

    }

    public static PrintService findPrintService(String printerName) {
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        for (PrintService printService : printServices) {
            if (printService.getName().trim().equals(printerName)) {
                return printService;
            }
        }
        return null;
    }

    private void cashDrawerOpen() {

        byte[] open = {27, 112, 48, 55, 121};
//        byte[] cutter = {29, 86,49};
        String printer = this.posPrinter;
        PrintServiceAttributeSet printserviceattributeset = new HashPrintServiceAttributeSet();
        printserviceattributeset.add(new PrinterName(printer, null));
        PrintService[] printservice = PrintServiceLookup.lookupPrintServices(null, printserviceattributeset);
        if (printservice.length != 1) {
            LOG.severe("Printer not found");
        }
        PrintService pservice = printservice[0];
        DocPrintJob job = pservice.createPrintJob();
        DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
        Doc doc = new SimpleDoc(open, flavor, null);
        PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
        aset.add(new JobName("Document", null));
        try {
            job.print(doc, aset);
        } catch (PrintException ex) {
            LOG.severe(ex.getMessage());
        }
    }

    public List<Printer> getPrintList(String term) {
        if (term == null)
            term = "";
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        List<Printer> printers = new ArrayList<>();
        int id = 0;
        for (PrintService printService : printServices) {
            if (printService.getName().toUpperCase().contains(term.toUpperCase())) {
                Printer printer = new Printer(id, printService.getName());
                printers.add(printer);
                id++;
            }
        }
        return printers;
    }

    private void setPrinters() {
        try {
            Properties prop = new Properties();
            InputStream input = new FileInputStream(path + "printer.config.properties");
            prop.load(input);
            String pos = prop.getProperty("printer.pos");
            if (pos != null && !pos.equals(""))
                this.posPrinter = pos;
            String basic = prop.getProperty("printer.basic");
            if (basic != null && !basic.equals(""))
                this.printer = basic;
        } catch (Exception ignored) {
        }
    }

}

package com.codespot.printconnector.model;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class Invoice {

    private Long invoiceId;

    private Double tax;

    private Double total;

    private Double subTotal;

    private Double discountTtl;

    private String discountPct;

    private Customer customer;

    private List<PosProduct> products;

    private Date date;

    private String voucherType;

    private String voucherNo;

    private String balance;

    private String credit;

    private Type status;

    private User createdBy;


    public enum Type {
        PENDING,
        DRAFTED,
        PROCESSED,
        CANCELED,
        CLOSED,
        PAYMENT_PENDING;

        public boolean isDraft(){
            return this.equals(DRAFTED);
        }
    }
}


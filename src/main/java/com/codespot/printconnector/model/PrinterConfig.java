package com.codespot.printconnector.model;

import lombok.Data;

@Data
public class PrinterConfig {
    private String pos;
    private String basic;
    private String directory;
    private String modifier;
}
